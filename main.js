// Task list array
let tasks = [];

// Function to render the task list
function renderTasks() {
  const taskList = document.getElementById("taskList");
  taskList.innerHTML = "";

  tasks.forEach((task, index) => {
    const taskItem = document.createElement("div");
    taskItem.classList.add(
      "flex",
      "items-center",
      "justify-between",
      "mb-2",
      "p-2",
      "border",
      "border-gray-300",
      "rounded-md"
    );

    const taskText = document.createElement("span");
    taskText.textContent = task;
    taskText.classList.add("text-lg");

    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.classList.add(
      "px-2",
      "py-1",
      "bg-red-500",
      "text-white",
      "rounded-md",
      "hover:bg-red-600",
      "focus:outline-none",
      "focus:ring-2",
      "focus:ring-red-500"
    );
    deleteButton.addEventListener("click", () => {
      deleteTask(index);
    });

    taskItem.appendChild(taskText);
    taskItem.appendChild(deleteButton);
    taskList.appendChild(taskItem);
  });
}

// Function to add a new task
function addTask(event) {
  event.preventDefault();
  const taskInput = document.getElementById("taskInput");
  const task = taskInput.value.trim();

  if (task !== "") {
    tasks.push(task);
    taskInput.value = "";
    renderTasks();
  }
}

// Function to delete a task
function deleteTask(index) {
  tasks.splice(index, 1);
  renderTasks();
}

// Event listener for form submission
const addTaskForm = document.getElementById("addTaskForm");
addTaskForm.addEventListener("submit", addTask);

// Initial rendering of tasks
renderTasks();
